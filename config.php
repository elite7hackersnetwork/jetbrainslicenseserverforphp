<?php

// Configuration file

return new class extends \StdClass
{
    public $rsa_sign_key = __DIR__ . '/sign_key.pem';
};