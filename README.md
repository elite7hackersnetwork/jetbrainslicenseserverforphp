# JetBrainsLicensingServer

Requires >= PHP7 with OpenSSL extension enabled, Composer for dependency management and auto-loading

Highly versatile design allows extending classes to add extra functionality (eg. license verification against internal database).



### Setup
Directory root: public_html/

Run:
```
cd /path/to/project
composer install
```

####  Rewrite configuration - All requests to the router

Nginx
```
location / {
    try_files $uri $uri/ /path/to/public_html/router.php?$args;
}

```

Apache
```         
<IfModule mod_rewrite.c>
    Options -MultiViews

    RewriteEngine On
    RewriteBase /
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ router.php [L]
</IfModule>
```
Please note that Apache .htaccess file is already in place.

### Data processing
The following is supplied by PhpStorm upon request:
```
    [buildDate] => 20170719
    [buildNumber] => 2017.2.4 Build PS-172.4155.41
    [clientVersion] => 4
    [hostName] => localhost
    [machineId] => xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    [productCode] => xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    [productFamilyId] => xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    [salt] => 1510401286729
    [secure] => false
    [userName] => xzero
    [version] => 2017200
    [versionNumber] => 2017200
```
Of which only 'userName' and 'salt' are used by server for basic operation. (every licensing request is approved)

### Configuration
config.php is used for configuration