<?php

use Klein\Klein;
use Klein\Request;
use Klein\Response;
use Klein\ServiceProvider;
use NorthernLights\JetBrainsLicensing\PrivateKeyFile;
use NorthernLights\JetBrainsLicensing\Server as LicensingServer;

require __DIR__ . '/../vendor/autoload.php';

$config = require(__DIR__ . '/../config.php');

$klein = new Klein(); // Init router

$klein->respond('/', function (Request $request, Response $response, ServiceProvider $service) {
    $response->json([
        'timestamp'   => time(),
        'description' => 'JetBrains License Server'
    ]);
});

$klein->respond(
    '/rpc/[:action].action',
    function (Request $request, Response $response, ServiceProvider $service) use ($config) {

        $pkf             = new PrivateKeyFile($config->rsa_sign_key); // Prepare key file
        $licensingServer = new LicensingServer($request, $pkf);

        $response->header('Content-Type', 'application/xml');
        $response->body($licensingServer->getResponse());
    }
);


$klein->dispatch();
