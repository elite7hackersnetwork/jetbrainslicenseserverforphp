<?php

namespace NorthernLights\JetBrainsLicensing\Exception;

use \Exception as DefaultException;

/**
 * Class PrivateKeyFileException
 * @package NorthernLights\JetBrainsLicensing\Exception
 */
class PrivateKeyFileException extends DefaultException
{

}
