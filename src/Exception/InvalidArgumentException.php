<?php

namespace NorthernLights\JetBrainsLicensing\Exception;

use \InvalidArgumentException as DefaultInvalidArgumentException;

/**
 * Class InvalidArgumentException
 * @package NorthernLights\JetBrainsLicensing\Exception
 */
class InvalidArgumentException extends DefaultInvalidArgumentException
{
}
