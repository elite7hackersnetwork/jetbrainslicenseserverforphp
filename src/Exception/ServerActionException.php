<?php

namespace NorthernLights\JetBrainsLicensing\Exception;

use \Exception as DefaultException;

/**
 * Class ServerActionException
 * @package NorthernLights\JetBrainsLicensing\Exception
 */
class ServerActionException extends DefaultException
{
}
