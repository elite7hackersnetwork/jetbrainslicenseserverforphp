<?php

namespace NorthernLights\JetBrainsLicensing\Exception;

use \Exception as DefaultException;

/**
 * Class RsaException
 * @package NorthernLights\JetBrainsLicensing\Exception
 */
class RsaException extends DefaultException
{

}
