<?php

namespace NorthernLights\JetBrainsLicensing\Exception;

use \Exception as DefaultException;

/**
 * Class XmlFactoryException
 * @package NorthernLights\JetBrainsLicensing\Exception
 */
class XmlFactoryException extends DefaultException
{
}
