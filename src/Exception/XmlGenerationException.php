<?php

namespace NorthernLights\JetBrainsLicensing\Exception;

use \Exception as DefaultException;

/**
 * Class XmlGenerationException
 * @package NorthernLights\JetBrainsLicensing\Exception
 */
class XmlGenerationException extends DefaultException
{
}
